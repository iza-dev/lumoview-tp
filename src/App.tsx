import { Box, Button, Text } from '@chakra-ui/react';
import { Canvas } from '@react-three/fiber';
import { Suspense, useRef } from 'react';
import { Mesh } from 'three';
import CameraControls from './components/CameraControls';
import Scene3D from './components/Scene3D';
import SkyBox from './components/world/SkyBox';
import useDetectionAndBlurring from './hooks/useDetectionAndBlurring';

function App() {
  const sphereRef = useRef<Mesh>(null);
  const { loading, processing, detectAndBlurFaces } = useDetectionAndBlurring();

  const handleClick = () => {
    if (loading === false) detectAndBlurFaces(sphereRef);
  };

  return (
    <Box position={'relative'}>
      <Box position={'absolute'} zIndex={10} top={10} right={10}>
        <Button onClick={handleClick}>
          {loading ? 'Loading ML models' : 'Detect faces and blur'}
        </Button>
        <Text backgroundColor={'azure'}>{processing ? 'Processing...' : ''}</Text>
      </Box>
      <Suspense fallback={<Text>Loading the 360° experience ...</Text>}>
        <Canvas camera={{ position: [0, 0, 50], fov: 50, zoom: 0.9 }}>
          <CameraControls />
          <SkyBox ref={sphereRef} />
          <Scene3D />
        </Canvas>
      </Suspense>
    </Box>
  );
}

export default App;

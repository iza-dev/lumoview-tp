import * as faceapi from 'face-api.js';
import { useCallback, useEffect, useState } from 'react';
import { canvasRGBA } from 'stackblur-canvas';
import { Mesh, MeshBasicMaterial } from 'three';

const useDetectionAndBlurring = () => {
  const [loading, setLoading] = useState(false);
  const [processing, setProcessing] = useState(false);

  useEffect(() => {
    loadModels();
  }, []);

  const loadModels = useCallback(async () => {
    setLoading(true);
    await faceapi.loadSsdMobilenetv1Model('/models');
    await faceapi.loadFaceLandmarkModel('/models');
    await faceapi.loadFaceRecognitionModel('/models');
    await faceapi.loadFaceExpressionModel('/models');
    setLoading(false);
  }, []);

  const detectAndBlurFaces = useCallback(async (sphereRef: React.MutableRefObject<Mesh | null>) => {
    setProcessing(true);

    if (sphereRef.current) {
      const material = sphereRef.current.material as MeshBasicMaterial;
      const texture = material.map;

      const canvas = document.createElement('canvas');
      const context = canvas.getContext('2d');
      canvas.width = texture?.image.width;
      canvas.height = texture?.image.height;
      context?.drawImage(texture?.image, 0, 0);

      const detections = await faceapi
        .detectAllFaces(canvas)
        .withFaceLandmarks()
        .withFaceExpressions();

      detections.forEach(detection => {
        const { x, y, width, height } = detection.detection.box;
        //blur faces
        canvasRGBA(canvas, x, y, width, height, 10);
      });

      const dataUrl = canvas.toDataURL();

      texture!.image.src = dataUrl;
      texture!.needsUpdate = true;

      setProcessing(false);
    }
  }, []);

  return { detectAndBlurFaces, loading, processing };
};

export default useDetectionAndBlurring;

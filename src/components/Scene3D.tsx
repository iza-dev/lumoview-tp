const Scene3D = () => {
  return (
    <>
      <ambientLight />
      <pointLight args={[2, 2, 2]} />
    </>
  );
};

export default Scene3D;

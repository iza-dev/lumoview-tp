import { OrbitControls } from '@react-three/drei';
import { useFrame } from '@react-three/fiber';
import { useRef } from 'react';
import type { OrbitControls as OrbitControlsImpl } from 'three-stdlib';

const CameraControls = () => {
  const controls = useRef<OrbitControlsImpl>(null);

  useFrame(_ => {
    if (controls.current) controls.current.update();
  });

  return <OrbitControls ref={controls} enableZoom maxDistance={70} />;
};

export default CameraControls;

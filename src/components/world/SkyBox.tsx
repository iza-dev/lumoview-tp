import { useLoader } from '@react-three/fiber';
import { forwardRef, useEffect } from 'react';
import { BackSide, Mesh, MeshBasicMaterial, TextureLoader } from 'three';

const SkyBox = forwardRef<Mesh | null>((_props, ref) => {
  const lowQualityTexture = useLoader(TextureLoader, '/image_test_low.jpg');

  useEffect(() => {
    const highQualityTexture = new TextureLoader().load('/image_test.jpg', texture => {
      if (ref && 'current' in ref && ref.current) {
        const material = ref.current.material as MeshBasicMaterial;

        material.needsUpdate = true;
        material.map = texture;
      }
    });

    return () => {
      highQualityTexture.dispose();
    };
  }, []);

  return (
    <>
      <mesh ref={ref}>
        <sphereGeometry attach="geometry" args={[60, 120, 120]} />
        <meshBasicMaterial attach="material" map={lowQualityTexture} side={BackSide} />
      </mesh>
    </>
  );
});

export default SkyBox;
